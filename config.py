from router_config import RouterConfig

def parseconfig(cfg_file_path):
    """
    Parse the router config, return a RouterConfig object
    Returns none if config file is invalid
    """
    cfg_file = open(cfg_file_path)
    router_config_args = maketokens(cfg_file)

    routing_table = {int(router_config_args[0]):(int(router_config_args[0]), None, None, 1, 0)}
    router_config = RouterConfig(
        router_config_args[0],
        router_config_args[1],
        router_config_args[2],
        [routing_table])

    return router_config


def maketokens(cfg_file):
    """
    Breaks the config file into a list of strings
    """
    router_config_args = [0, [], []]
    for line in cfg_file:
        tokens = line.split(" ")
        router_config_args = parsetokens(tokens, router_config_args)
    return router_config_args


def parsetokens(tokens, router_config_args):
    """
    Takes a list of tokens, puts them into a list
    """
    for i in range(1, len(tokens)):

        if tokens[-1] == ",":
            tokens = tokens[:-1]
        if tokens[i] != "" and tokens[i] != "\n":
            #strip the unneeded characters off of the parameters
            tokens[i] = tokens[i].rstrip("\n")
            tokens[i] = tokens[i].rstrip(",")
            if tokens[0] == "router-id":
                router_config_args[0] = int(tokens[i])
            if tokens[0] == "input-ports":
                router_config_args[1].append(int(tokens[i]))
            if tokens[0] == "outputs":
                router_config_args[2].append(tokens[i])

    return router_config_args