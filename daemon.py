import sys
import select
import socket
import time
import struct
from rip_packet import RipPacket
from rip_entry import RipEntry
from config import parseconfig
import subprocess as sp

TIMEOUT_TIMER = 35
GARBAGE_COLLECT_TIMER = 5

ROUTING_TABLE_SEND_INTERVAL = 30
ROUTING_TABLE_UPDATE_INTERVAL = 1
DISPLAY_UPDATE_INTERVAL = 1
SOCKET_READ_TIMEOUT = 1

def get_input_port_sockets(router_config):
    """
        Return a list of sockets bound to the ports specified
        in the router configuration
    """
    sockets = []

    for portno in router_config.get_in_ports():
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(('localhost', int(portno)))
        sockets.append(sock)

    return sockets


def send_response_on_socket(rip_packet, sock, port):
    """
    Send the rip_packet to a neighbour
    :rip_packet: The rip packet to send
    :sock: The socket to send the packet on
    :port: The port to send the packet to
    """
    _, writable, _ = select.select([], [sock], [])

    if writable[0] == sock:
        sock.sendto(rip_packet.make_packet(), ("localhost", port))


def parse_packet_data(packet):
    """
    Parse the packet and return a RipPacket
    """
    offset = 0
    command, version, _ = struct.unpack_from("BBH", packet, offset)
    offset += struct.calcsize("BBH")

    rip_packet = RipPacket(command, version)

    for i in range(0, 25):
        AF_identifier, _, ipv4, _, next_hop, metric = struct.unpack_from("HHIIII", packet, offset)
        offset += struct.calcsize("HHIIII")

        rip_packet.add_entry(RipEntry(AF_identifier, ipv4, metric, next_hop))

    return rip_packet


def close_sockets(sockets):
    """
    Takes a list of sockets and closes them
    :param sockets: A list of sockets
    """
    for sock in sockets:
        sock.close()


def get_router_from_argv(argv):
    """
    Reads router config specified in router cfg and loads it
    :return: router or None if there was an error
    """
    if len(argv) == 1:
        if argv[0] == "-h" or argv[0] == "--help":
            print("Usage: python3 daemon.py [config file path]")
            quit(1)
        else:
            router_config = parseconfig(argv[0])
            success("Router with ID: " + str(router_config.get_router_id()) + " created!")
            return router_config

    elif len(argv) < 1:
        error("Not enough arguments (config file needed)")
        quit(1)

    else:
        error("Error: Too many arguments")
        quit(1)


def update_routing_table(routing_table, this_router_id, socket, router_config):
    """
    Decrements timers, and sends update messages if an entry has been changed
    Must be called every second.
    """
    routers_to_delete = [] #Entries that need to be deleted

    for router_id in routing_table.keys():
        # Don't send update messages to this router
        if this_router_id == router_id:
            continue

        # Check timeout hasn't expired
        # a negative timeout means the timeout shouldn't be decremented
        if routing_table[router_id][4] == 0:
            routing_table[router_id][1] = 16
            send_routing_table(routing_table, socket, router_config)

            # Garbage collection has expired
            if routing_table[router_id][5] == 0:
                routers_to_delete.append(router_id)

            else:
                # decrement garbage collection
                routing_table[router_id][5] -= 1

        else:
            # Decrement timeout
            routing_table[router_id][4] -= 1

        # Entry was recently updated, send routing update
        if routing_table[router_id][3] == True:
            send_routing_table(routing_table, socket, router_config)
            routing_table[router_id][3] = False
            routing_table[router_id][4] = TIMEOUT_TIMER
            routing_table[router_id][5] = GARBAGE_COLLECT_TIMER

    # Delete routers that are dead
    for router_id in routers_to_delete:
        routing_table.pop(router_id)
        #del routing_table[router_id]

    return routing_table


def is_directly_connected(router_id, router_config):
    """
    Return whether or not :router_id: is directly connected
    to this router
    """
    for output in router_config.get_outputs():
        if int(output[2]) == router_id:
            return True

    return False


def is_connected_neighbour(entry, router_config):
    """
    Return whether or not this router is directly connected to us, and the message 
    is coming from that router
    """
    return entry.next_hop == entry.ipv4 and is_directly_connected(entry.next_hop, router_config)


def process_input_packet(packet, routing_table, router_config):
    """
    Update routing table when a new packet is recieved
    """
    for entry in packet.entries:
        # AF_identifier will be zero if 
        # there are no more entries
        if entry.AF_identifier == 0:
            break

        # If this entry is a neighbour connected directly to this router, and an
        # entry already exists in the table. This serves 2 purposes
        #   1) This lets us know we are still connected to our neighbours
        #   2) We can update the metrics when a router has gone offline, and comes back
        #      before its entry has been removed 
        if is_connected_neighbour(entry, router_config) and routing_table.get(entry.next_hop) != None:
            routing_table[entry.next_hop][4] = TIMEOUT_TIMER
            routing_table[entry.next_hop][5] = GARBAGE_COLLECT_TIMER 

            metric = router_config.get_metric_for_neighbour(entry.next_hop)

            if metric < routing_table[entry.ipv4][1]:
                routing_table[entry.ipv4][1] = metric
                routing_table[entry.ipv4][2] = entry.next_hop
                routing_table[entry.ipv4][3] = True
                continue

        # Ignore entry
        if entry.metric >= 16:
            continue

        # We already have an entry in the routing table 
        # for this router
        if routing_table.get(entry.ipv4) != None:
            if routing_table.get(entry.next_hop) == None:
                metric = 16

            else:
                # Calculate new metric
                metric = entry.metric + routing_table[entry.next_hop][1]

            if metric >= 16:
                continue

            # The next hop is still the same as what's in the 
            # routing table
            if entry.next_hop == routing_table[entry.ipv4][2]:
                routing_table[entry.ipv4][4] = TIMEOUT_TIMER
                routing_table[entry.ipv4][5] = GARBAGE_COLLECT_TIMER

                # We have a new metric
                if routing_table[entry.ipv4][1] != metric:
                    routing_table[entry.ipv4][1] = metric
                    routing_table[entry.ipv4][3] = True # A update should be sent

            # We have a new router with a better metric
            elif routing_table[entry.ipv4][1] > metric:
                routing_table[entry.ipv4][1] = metric
                routing_table[entry.ipv4][2] = entry.next_hop
                routing_table[entry.ipv4][3] = True
                routing_table[entry.ipv4][4] = TIMEOUT_TIMER
                routing_table[entry.ipv4][5] = GARBAGE_COLLECT_TIMER
            
        # We didn't have an entry in the table for this router
        else:
            # We don't have the routers next hop in the routing table
            if routing_table.get(entry.next_hop) == None:
                # The router is our neigbour and we have a metric for it
                if router_config.get_metric_for_neighbour(entry.ipv4) >= 0:
                    metric = router_config.get_metric_for_neighbour(entry.next_hop)
                else:
                    metric = entry.metric

            else:
                metric = routing_table[entry.next_hop][1] + entry.metric

            # Add router to our routing table
            routing_table[entry.ipv4] = [entry.ipv4, metric, entry.next_hop, True, TIMEOUT_TIMER, GARBAGE_COLLECT_TIMER]

    return routing_table


def init_routing_table(router_config):
    """
    Return an initial routing table based on the
    configuration file

    Table format:
    0 = IPv4
    1 = Metric
    2 = Next Hop (None if directly connected)
    3 = Recently Updated
    4 = Timeout Timer (-1 if self)
    5 = Garbage Collection Timer (-1 if self)
    """
    routing_table = dict()
    this_router_id = router_config.get_router_id()

    # Our entry in the routing table
    routing_table[this_router_id] = [this_router_id, 0, -1, False, -1, -1]

    # Add our peers to the routing table
    for peer_router_config in router_config.get_outputs():
        router_id = int(peer_router_config[2])
        metric = int(peer_router_config[1])
        routing_table[router_id] = [router_id, metric, router_id, False, TIMEOUT_TIMER, GARBAGE_COLLECT_TIMER]

    return routing_table


def print_routing_table(routing_table, this_router_id=None):
    """
    Print the routing table
    """
    tmp = sp.call('clear',shell=True)
    if this_router_id != None:
        print("\n\nRouting Table for router #" + str(this_router_id))

    print("|| Router ID || Metric || Next Hop || Route Changed || Timeout ||  GC  ||")
    print("||-----------||--------||----------||---------------||---------||------||")

    for router_id in routing_table.keys():
        router = routing_table[router_id]
        print("|| {:<9} || {:<7}|| {:<9}|| {:<14}|| {:<8}|| {:<5}||".format(router_id, router[1], router[2], router[3], router[4], router[5]))


def error(error_message):
    """
    prints an error message
    """
    print("\x1B[31mERROR: " + error_message + "\x1B[0m")


def success(success_message):
    """
    prints a success message
    """
    print("\x1B[32mSUCCESS: " + success_message + "\x1B[0m")


def send_routing_table(routing_table, socket, router_config):
    """
    Send the routing table to all neighbours
    """
    for portno, router_id in router_config.get_neighbours():
        packet = RipPacket(command=2,version=2)
        # Dont send update to self
        if (router_config.get_router_id() == router_id):
            continue

        for entry in routing_table.keys():
            _, metric, _, _, _, _ = routing_table[entry]

            # If this entry was learned from neighbour, set 
            # the metric to infinite
            if routing_table[entry][2] == router_id:
                metric = 16

            # If timeout has expired, set the metric to infinite
            if routing_table[entry][4] == 0:
                metric = 16
                
            packet.add_entry(RipEntry(1, entry, metric, router_config.get_router_id()))

        send_response_on_socket(packet, socket, portno)


def read_sockets(sockets, routing_table, router_config):
    """
    Read routing packets from the sockets, then send them 
    for processing
    """
    for sock in sockets:
        data, addr = sock.recvfrom(1024)

        recv = parse_packet_data(data)
        routing_table = process_input_packet(recv, routing_table, router_config)

    return routing_table

def main(argv):
    if argv != None:
        router_config = get_router_from_argv(argv)

        # Routing config file was invalid or not found
        if router_config is None:
            error("Bad config file")
            quit(1)
    else:
        print("use -h or --help for help")
        quit()

    # Initialise routing table and sockets
    routing_table = init_routing_table(router_config)
    print_routing_table(routing_table, router_config.get_router_id())

    input_sockets = get_input_port_sockets(router_config)

    last_table_update = time.time()
    last_update_sent = time.time()
    last_display = time.time()

    while True:
        # Send routing messages to neighbours
        if time.time() - last_update_sent > ROUTING_TABLE_SEND_INTERVAL:
            send_routing_table(routing_table, input_sockets[0], router_config)
            last_update_sent = time.time()

        # Update our routing table (timers etc)
        if time.time() - last_table_update > ROUTING_TABLE_UPDATE_INTERVAL:
            update_routing_table(routing_table, router_config.get_router_id(), input_sockets[0], router_config)
            last_table_update = time.time()

        # Refresh displayed routing table
        if time.time() - last_display > DISPLAY_UPDATE_INTERVAL:
            print_routing_table(routing_table, router_config.get_router_id())
            last_display = time.time()

        readable, _, _ = select.select(input_sockets, [], [], SOCKET_READ_TIMEOUT)
        routing_table = read_sockets(readable, routing_table, router_config)


if __name__ == "__main__":
    main(sys.argv[1:])