class RipEntry:
    def __init__(self, AF_identifier, ipv4, metric, next_hop):
        self.AF_identifier = AF_identifier
        self.ipv4 = ipv4
        self.metric = metric
        self.next_hop = next_hop

    def get(self):
        """
        Returns a tuple containing this routers information
        """
        return (self.AF_identifier, self.ipv4, self.metric, self.next_hop)

    def print_entry(self):
        """
        Print the RIP entry
        """
        print("\t#### RIP Entry ####")
        print("\t\tAF_identifier: " + str(self.AF_identifier))
        print("\t\tIP: " + str(self.ipv4))
        print("\t\tMetric: " + str(self.metric))
        print("\t\tNext Hop: " + str(self.next_hop))