import struct
from rip_entry import RipEntry

class RipPacket:
    def __init__(self, command, version=1):
        self.command = command
        self.version = version
        self.entries = []

    def add_entry(self, entry):
        """
        Add a RipEntry to this packet
        """
        self.entries.append(entry)

    def make_packet(self):
        """
        Pack the data into a bytearray
        :return: A bytearray containing the routing message
        """
        byte_arr = bytearray(struct.calcsize("BBH") + struct.calcsize("HHIIII") * 25)
        arr_pos = 0

        struct.pack_into("BBH", byte_arr, arr_pos, self.command, self.version, 0x00)
        arr_pos += struct.calcsize("BBH")

        for entry in self.entries:
            AF_identifier, ipv4, metric, next_hop = entry.get()
            struct.pack_into("HHIIII", byte_arr, arr_pos, AF_identifier, 0x00, int(ipv4), 0x0000, next_hop, int(metric))
            arr_pos += struct.calcsize("HHIIII")

        return byte_arr

    def print_packet(self):
        print("#### Rip Packet ####")
        print("\tCommand: " + str(self.command))
        print("\tVersion: " + str(self.version))

        for entry in self.entries:
            if entry.ipv4 != 0:
                entry.print_entry()