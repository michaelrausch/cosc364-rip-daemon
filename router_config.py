class RouterConfig:
    """
    contains the router config information
    """
    def __init__(self, router_id, in_ports, outputs, routing_table):
        formatted_outputs = []
        for output in outputs:
            formatted_outputs.append(output.split("-"))
        self.router_id = router_id
        self.in_ports = in_ports
        self.outputs = formatted_outputs
        self.routing_table = routing_table

    def get_router_id(self):
        """
        :returns: the router id number
        """
        return int(self.router_id)


    def get_in_ports(self):
        """
        :returns: the input ports for the router
        """
        return self.in_ports


    def get_outputs(self):
        """
        :returns: the outputs for the router
        """
        return self.outputs


    def get_neighbours_ports(self):
        """
        Get a list of ports that belong to our
        neighbours
        """
        port_list = []
        for output in self.outputs:
            port_list.append(int(output[0]))

        return port_list


    def get_neighbours(self):
        """
        Get a list of tuples containing neigbours ports and
        router ids
        """ 
        neighbours = []
        for output in self.outputs:
            if (int(output[2]) != int(self.router_id)):
                neighbours.append((int(output[0]), int(output[2])))

        return neighbours


    def get_metric_for_neighbour(self, router_id):
        """
        Gets the metric for :router_id: if it is a neighbour
        Returns -1 if router_id is not a neighbour
        """
        for output in self.get_outputs():
            if int(output[2]) == int(router_id):
                return int(output[1])

        return -1